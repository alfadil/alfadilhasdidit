from django.shortcuts import render,redirect,get_object_or_404
from .forms import PostForm
from django.utils import timezone
from .models import Post,Category,Vender
from mysite import settings
import re

def post_details(request,pk):
	post = get_object_or_404(Post,pk = pk)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	if request.method == "GET":
		url = post.image.url
	return render(request, 'ealan/post.html', {'form': post,'urltoimage' : url,'categories':categories,'venders':venders})

def post_all(request):
	posts = Post.objects.all().order_by("-date")[:25]
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_cat(request,category):
	posts = Post.objects.filter(category=category)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_ven(request,vender):
	posts = Post.objects.filter(vender=vender)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_search(request):
	search_query = request.GET.get('search', None)
	posts = Post.objects.filter(title__contains = search_query)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_ven_info(request,pk):
	vender = get_object_or_404(Vender,pk = pk)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	#if request.method == "GET":
	#	url = post.image.url
	return render(request, 'ealan/venderinfo.html', {'form': vender,'categories':categories,'venders':venders})
