$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
	$('#magnify_image').loupe({
		  width: 200, // width of magnifier
		  height: 150, // height of magnifier
		  loupe: 'loupe' // css class for magnifier
		});
	$('.tree-toggle').click(function () {
	$(this).parent().children('ul.tree').toggle(200);
	});
});
