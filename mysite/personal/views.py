from django.shortcuts import render

def index(request):
	return render(request,'personal/index.html')

def contact(request):
	return render(request,'personal/basic.html',{'content':['if you want to contact me send e-mail to','alfadil.tabar@gmail.com']})