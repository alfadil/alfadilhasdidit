from django.forms import ModelForm
from .models import Post

class  PostForm(ModelForm):
	"""the form of the post table"""
	class Meta:
		model = Post
		fields  =('title','body',)
			
		