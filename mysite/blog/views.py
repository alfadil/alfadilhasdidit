from django.shortcuts import render,redirect,get_object_or_404
from .forms import PostForm
from django.utils import timezone
from .models import Post

def post_new(request):
	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.date = timezone.now()
			post.save()
			return redirect("post_list")
		else:
			return render(request, 'blog/post_edit.html', {'form': form})
	else : 
		form = PostForm()
		return render(request, 'blog/post_edit.html', {'form': form})

def post_edit(request,pk):
	post = get_object_or_404(Post,pk = pk)
	if request.method == "POST":
		form = PostForm(request.POST,instance = post)
		if form.is_valid():
			post = form.save(commit=False)
			post.date = timezone.now()
			post.save()
			return redirect("post_list")
	else : 
		form = PostForm(instance = post)
	return render(request, 'blog/post_edit.html', {'form': form})

def post_delete(request,pk):
	Post.objects.get(id = pk).delete()
	return redirect("post_list")
